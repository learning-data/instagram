from sqlalchemy import Column, String, Date, Integer, Numeric, TIMESTAMP
from sqlalchemy.sql import func
from base import Base

class InstagramAccount(Base):
    __tablename__ = 'instagram_account'
    id = Column(Integer, primary_key = True)
    username = Column(String(32))
    password = Column(String(255))
    name = Column(String(255))
    is_blocked = Column(Integer)
    created_at = Column(TIMESTAMP)
    is_used = Column(Integer)









