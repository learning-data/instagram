from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import selenium.common.exceptions
from elasticsearch import Elasticsearch
import elasticsearch6
from sqlalchemy import update

from elasticsearch.exceptions import ConnectionError, TransportError
from elasticsearch.helpers import BulkIndexError


from base import *
from config import *
from datamodel_account import *
from datamodel_aop import *

import time
import os
import logging
import traceback
import requests
import json
import re
from datetime import datetime as dt
import logger
import pandas as pd
import pytz
import pickle
# from elasticsearch import Elasticsearch, ElasticsearchWarning

logging.basicConfig(filename='scraper.log', encoding='utf-8')
format = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
ch = logging.StreamHandler()
ch.setFormatter(format)
logger = logging.getLogger(__name__)
logger.addHandler(ch)

headers = {
            'authority': 'www.instagram.com',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
            'accept-language': 'en-GB,en;q=0.9,en-US;q=0.8,id;q=0.7,ms;q=0.6',
            'cache-control': 'max-age=0',
            'cookie': 'dpr=2; ig_nrcb=1; ig_did=26795726-609C-4D93-A325-351B6F56B247; mid=ZCY7rAAEAAH5L0DP7pxBgage4z67; csrftoken=EyCZDHAhRI1seQqgs0dw8G30KdT5qxHm; ds_user_id=31044768; datr=ODwmZOLRjuTWvrwDSAsG7mdV; shbid="11221\\05431044768\\0541712107608:01f7cd4496acaf60386b8fd75df5f7d1d4a581ca388ac49010ad536fd48bc6ba574d3db2"; shbts="1680571608\\05431044768\\0541712107608:01f74d5dbb8c9740d63a37c380ba7f50a512197c0598060e0f5ea4e045454abe27e4ac80"; sessionid=31044768%3ACTd14MGJTuBDan%3A21%3AAYdbNwAxLtFLp7msw6k2UwhYXj3Ya6ehAxrI2oEhJw; rur="PRN\\05431044768\\0541712116231:01f7230d379ac679afe1501d30bef6be5716305d0d1efafe3aa4d78b63c879995e433a9a"; csrftoken=KfiOqJhuLukfCpbAEjOOYvo3mSw88Orl; ds_user_id=31044768; ig_did=155F10B7-C253-47DA-AC82-30CE0C05062E; ig_nrcb=1; mid=ZCt96AAEAAF40C-mLw3yxEtDNMwd; rur="PRN\\05431044768\\0541712116729:01f7a06d6144518bc85307f40ca295267b8bf1c2ab47b7d68270e8348bceb6543f03c4d7"',
            'sec-ch-prefers-color-scheme': 'light',
            'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'none',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
            'viewport-width': '1680'
        }


proxies = {
    # "http": configProxies.HTTP_PROXY,
    # "https": configProxies.HTTPS_PROXY
    }

options = webdriver.ChromeOptions()
options.add_argument("--user-data-dir=chrome-data")
options.add_argument('--headless')
# options.add_argument("start-maximized")
options.add_argument("disable-infobars")
options.add_argument("--disable-extensions")
options.add_argument("--disable-gpu")
# options.add_argument("--disable-dev-shm-usage")
# options.add_argument("--no-sandbox")
service = webdriver.chrome.service.Service(ChromeDriverManager().install())
driver = webdriver.Chrome(service=service,options = options)
cwd = os.getcwd()


def get_account():
    # create a session
    session = SessionAccount()
    # call users from the database
    query = session.query(InstagramAccount.username, InstagramAccount.password).\
        filter_by(is_blocked=0, is_used=1).all()
    for row in query:
        account = row[:2]
    ig_account = (account[0],account[1])
    # close the session
    session.close()
    # return the list of users
    return ig_account
    

def login():
    driver.maximize_window
    driver.get("https://www.instagram.com")

    ig_acc = get_account()
    USERNAME = ig_acc[0]
    PASSWORD = ig_acc[1]
    try:
        username_field = WebDriverWait(driver,10)\
            .until(EC.presence_of_element_located((By.NAME, 'username')))
        username_field.send_keys(USERNAME)
        time.sleep(2)
        print(username_field)

        password_field = WebDriverWait(driver,10)\
            .until(EC.presence_of_element_located((By.NAME, 'password')))
        password_field.send_keys(PASSWORD)
        time.sleep(5)
        print(password_field)

        loggin_button = WebDriverWait(driver,10)\
            .until(EC.presence_of_element_located((By.XPATH, '//*[@id="loginForm"]/div/div[3]/button')))
        loggin_button.click()
        print("Login To Instagram Account ....")
        print(loggin_button)
        time.sleep(5)
        pickle.dump(driver.get_cookies(), open("instagram_cookies.pkl", "wb"))
        print('Success to login and Create Cookies')
    except Exception as e:
        logging.error(traceback.format_exc())
    try:
        not_now = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//div[@role='button']")))
        not_now.click()
        time.sleep(5)
        print('not now 1',not_now)
    except:
        pass
    
    try:
        not_now2 = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//button[normalize-space()='Not Now']")))
        not_now2.click()
        print('not now 2',not_now2)
        time.sleep(5)
    except:
        pass
    
def setsession():
    cookies = pickle.load(open("instagram_cookies.pkl", "rb"))
    for cookie in cookies:
        driver.add_cookie(cookie)
    cookies = driver.get_cookies()
    return cookies

def request_json(url: str):
    # visit the URL with Selenium to get the cookies
    driver.get(url)
    cookies = driver.get_cookies()

    # create a requests session and add cookies to the header
    session = requests.Session()
    for cookie in cookies:
        session.cookies.set(cookie['name'], cookie['value'])

    # make the request with the cookies
    response = session.get(url)
    time.sleep(2)
    return response.json()
    

def get_account_target():
    # create a session
    session = SessionUser()
    # call users from the database
    query = session.query(Instagram.instagram_username) \
                .filter(Instagram.type == 'account') \
                .filter(Instagram.instagram_user_id != None) \
                .filter(Instagram.status == 1) \
                .filter((Instagram.is_valid == 1) | (Instagram.is_valid == None)) \
                .order_by(Instagram.updated.desc(), Instagram.id.desc()) \
                .all()
    session.close()
    # return the list of users
    return query  

# def get_location_info(lat, long):
#     api_key = '1d508c66edd7444d8f8d76874ddcdb72'
#     url = f"https://api.opencagedata.com/geocode/v1/json?q={lat}+{long}&key={api_key}"
#     response = requests.get(url,headers=headers).json()
#     location_info = {}
#     if 'results' in response:
#         results = response['results'][0]
#         location_info['province_id'] = results.get('components', {}).get('state_code', '')
#         location_info['province_name'] = results.get('components', {}).get('state', '')
#         location_info['city'] = results.get('components', {}).get('city', '')
#         # location_info['county'] = results.get('components', {}).get('county', '')
#         location_info['address'] = results.get('formatted', '')
#     location_info['lat'] = lat
#     location_info['long'] = long
#     return location_info

 #using openstreetmap.org/
def get_location_info(lat, long):
    url = f"https://nominatim.openstreetmap.org/reverse?lat={lat}&lon={long}&format=jsonv2"
    
    response = requests.get(url).json()
    location_info = {}
    if 'address' in response:
        address = response['address']
        location_info['province_id'] = address.get('state_code', '')
        location_info['province_name'] = address.get('state', '')
        location_info['city'] = address.get('city', '')
        location_info['county'] = address.get('county', '')
        location_info['address'] = address.get('road', '') + ', ' + address.get('suburb', '') + ', ' + address.get('city', '') + ', ' + address.get('state_district', '') + ', ' + address.get('postcode', '')
    location_info['lat'] = lat
    location_info['long'] = long
    return location_info

def convert_time(time):
    ts = int(time)
    converted = dt.utcfromtimestamp(ts)
    converted = converted.strftime('%Y-%m-%d %H:%M:%S')
    return converted

def parse_page_info(response_json) :
    top_level_key = "graphql" if "graphql" in response_json else "data"
    user_data = response_json[top_level_key].get("user", {})
    page_info = user_data.get("edge_owner_to_timeline_media", {}).get("page_info", {})
    return page_info

def parse_posts(response_json):
    since = configTime.since
    until = configTime.until
    top_level_key = "graphql" if "graphql" in response_json else "data"
    user_data = response_json[top_level_key].get("user", {})
    post_edges = user_data.get("edge_owner_to_timeline_media", {}).get("edges", [])
    
    posts = []
    for node in post_edges:
        post_json = node.get("node", {})
        id = post_json.get('id')
        shortcode = post_json.get("shortcode")
        timestamp = post_json.get("taken_at_timestamp")

        created = convert_time(timestamp)
        if since is not None and created < since:
            continue
        if until is not None and created > until:
            break
        
        posts.append({
            "id": int(id),
            "link": f"https://instagram.com/p/{shortcode}",
            "shortcode":shortcode,
            "created": created,
        })
    
    return posts

def parse_profile(response_json):
    top_level_key = "graphql" if "graphql" in response_json else "data"
    user_data = response_json[top_level_key].get("user")
    # post_edges = user_data.get("edge_owner_to_timeline_media", {}).get("edges", [])
    profile = []
    id = user_data.get('id')
    username = user_data.get('username')
    # full_name = user_data.get('full_name')
    # is_private = user_data.get('is_private')
    # profile_pic_url = user_data.get('profile_pic_url')
    follower = user_data.get('edge_followed_by').get('count')
    following = user_data.get('edge_follow').get('count')
    
    profile.append({
        "id": int(id),
        "username": username,
        "total_followers":follower,
        "total_following":following
    })
    return profile

def scrape_ig_post(username: str):
    limit = configTime.numberOfposts
    ig_profile_url = (f"https://www.instagram.com/{username}/?__a=1&__d=dis")
    print()
    print(ig_profile_url)

    response_json = request_json(f"{ig_profile_url}")
    print()
    # print(response_json)
    print()

    # get user_id from response to request next pages with posts
    user_id = response_json.get("graphql", {}).get("user", {}).get("id")
    if not user_id:
        print(f"User {username} not found.")
        return []
    # parse the first batch of posts from user profile response
    posts = parse_posts(response_json=response_json)
    page_info = parse_page_info(response_json=response_json)
    # get next page cursor
    end_cursor = page_info.get("end_cursor")
    has_next_page = page_info.get("has_next_page")
    has_next_page == True
    
    
    while end_cursor:
        posts_url = (
            f"https://instagram.com/graphql/query/?query_id=17888483320059182&id={user_id}&first=24&after={end_cursor}"
        )
        response_json = request_json(f"{posts_url}")
        time.sleep(10)
        if len(posts) >= limit:
            has_next_page = False
            break
        else:
            posts.extend(parse_posts(response_json=response_json))
        # page_info = parse_page_info(response_json=response_json)
        # end_cursor = page_info.get("end_cursor")
        
    return posts

def scrape_ig_profile(username: str):
    ig_profile_url = (f"https://www.instagram.com/{username}/?__a=1&__d=dis")
    
    response_json = request_json(f"{ig_profile_url}")
    profile = parse_profile(response_json=response_json) 
    return profile

# def connect_elasticsearch():
#     es = Elasticsearch([f'http://{configElasticsearch.HOST}:{configElasticsearch.PORT}'])
#     return es
def connect_elasticsearch():
    es = Elasticsearch(
        [f'http://{configElasticsearch.HOST}:{configElasticsearch.PORT}'],
        timeout=60,  # timeout set to 60 seconds
        max_retries=5,  # maximum retries set to 5
        retry_on_timeout=True,  # retry on timeout
        http_compress=True  # enable http compression
    )
    return es


def UploadDatatoES(filename):
    es = connect_elasticsearch()
    indexed_count = 0
    already_indexed_count = 0
    current_dir = os.getcwd()
    file_path = os.path.join(current_dir, filename)
    with open(file_path, 'r') as json_file:
        json_docs = json.load(json_file)
        print(json_docs.get('id'),json_docs.get('user').get('username'))
        try:
            # Retry failed connections up to 3 times
            for attempt in range(3):
                try:
                    doc_exists = es.exists(index=configElasticsearch.INDEX, doc_type="post", id=json_docs.get('id'))
                    if doc_exists:
                        already_indexed_count += 1
                        print("=" * 70)
                        print(f"Instagram Post with {json_docs.get('id')} ID has already been indexed")
                        print("=" * 70)
                        print()
                        return
                    else:
                        result = es.index(
                            index=configElasticsearch.INDEX, 
                            doc_type="post",
                            id=json_docs.get('id'),
                            body=json_docs
                        )
                        time.sleep(5)
                        print()
                        logger.setLevel(logging.INFO)
                        logger.info('indexed {}'.format(result))
                        indexed_count += 1 
                        return
                except ConnectionError as ce:
                    logger.warning(f"ConnectionError during get: {ce}")
                    if attempt == 2:
                        raise ce
                    time.sleep(2**attempt)

        except Exception as e:
            logger.warning(f"Failed indexing: {e}")
            raise e

def UploadDatatoESforComment(filename):
    es = connect_elasticsearch()
    indexed_count = 0
    already_indexed_count = 0
    current_dir = os.getcwd()
    file_path = os.path.join(current_dir, filename)
    with open(file_path, 'r') as json_file:
        json_docs = json.load(json_file)
        for ids in range(len(json_docs)):
            print()
            print('with id :',json_docs[ids].get('id'))
            try:
                # Retry failed connections up to 3 times
                for attempt in range(3):
                    try:
                        doc_exists = es.exists(index=configElasticsearch.INDEX, doc_type="post", id=json_docs[ids].get('id'))
                        if doc_exists:
                            already_indexed_count += 1
                            print("=" * 70)
                            print(f"Instagram Comment in Post with {json_docs[ids].get('id')} ID has already been indexed")
                            print("=" * 70)
                            print()
                            return
                        else:
                            result = es.index(
                                index=configElasticsearch.INDEX, 
                                doc_type="post",
                                id=json_docs[ids].get('id'),
                                body=json_docs[ids]
                            )
                            time.sleep(5)
                            print()
                            logger.setLevel(logging.INFO)
                            logger.info('indexed {}'.format(result))
                            indexed_count += 1 
                            return
                    except ConnectionError as ce:
                        logger.warning(f"ConnectionError during get: {ce}")
                        if attempt == 2:
                            raise ce
                        time.sleep(2**attempt)

            except Exception as e:
                logger.warning(f"Failed indexing Comment: {e}")
                raise e

# read data postfeed
def parse_feed():           
    # filename = "postfeed.json"
    file_path = os.path.join(cwd, "postfeed.json")
    # filepath = os.path.join(filename)
    with open(file_path, 'r') as json_file:
        values = json.load(json_file)
    
    # parsing data post from feed
    usertags = []
    if 'usertags' in values['items'][0]:
        usertags_dict = values['items'][0]['usertags']
    else:
        usertags_dict = {}
    jumlahtags = len(usertags_dict)
    if jumlahtags > 0:
        usertag = usertags_dict['in']
        for item in usertag:
            usertag_username = item['user']['username']
            usertags.append(usertag_username)
    
    user = {
        "id":values['items'][0]['user']['pk'],
        "full_name":values['items'][0]['user']['full_name'],
        "is_private":values['items'][0]['user']['is_private'],
        "profile_pic_id" : values['items'][0]['user']['profile_pic_id'],
        "profile_pic_url":values['items'][0]['user']['profile_pic_url'],
        "username":values['items'][0]['user']['username']
    }

    hashtags = None
    try:
        caption_text = values['items'][0]["caption"]["text"]
        hashtags = re.findall(r"#\w+", caption_text)
    except TypeError:
        hashtags = ''

    caption_text = ''
    try:
        caption_text = values['items'][0]["caption"]['text']
    except (KeyError, TypeError):
        pass

    caption = {
        "text": caption_text.encode('ascii', 'ignore').decode('unicode-escape').replace('\n', ' '),
        "content_type": 'post'
    }

    location = {}
    if 'location' is not None:
        try:
            coordinate = {
                "lat": values['items'][0]['lat'],
                "lon": values['items'][0]['lng']
            }
            location_info = get_location_info(coordinate['lat'], coordinate['lon'])
            location = {
                "address": location_info.get('address', ''),
                "city": location_info.get('city', ''),
                "coordinate": f"{coordinate['lat']}, {coordinate['lon']}",
                "lat":  values['items'][0]['lat'],
                "long": values['items'][0]['lng'],
                "name": values['items'][0]['location']['name'],
                "province_id": location_info.get('province_id', ''),
                "province_name": location_info.get('province_name', ''),
                "short_name": values['items'][0]['location']['short_name']
            }
        except:
            location = {}

    media_type = ''
    if values['items'][0]['media_type'] == 1:
        media_type = 'images'
    elif values['items'][0]['media_type'] == 2:
        media_type = 'video'
    elif values['items'][0]['media_type'] == 8:
        media_type = 'carousel'

    media = []
    url_image = ''
    url_video = ''

    if media_type == 'images':
        url_image = values['items'][0]['image_versions2']['candidates'][0]['url']
        media.append({
            'id' : values['items'][0]['id'],
            'type' : media_type,
            'image_url' : url_image,
            'video_url' : '',
        })
    elif media_type == 'video':
        url_video = values['items'][0]['video_versions'][0]['url']
        media.append({
            'id' : values['items'][0]['id'],
            'type' : media_type,
            'image_url' : '',
            'video_url' : url_video,
        })

    # elif 'carousel_media' in values:
    elif media_type == 'carousel':
            # media_url = values['carousel_media'][0]['image_versions2']['candidates'][0]['url']
        carousel_list = values['items'][0]['carousel_media']
        for n in range(len(carousel_list)):
            if 'image_versions2' in carousel_list[n]:
                url_image = carousel_list[n]['image_versions2']['candidates'][0]['url']
                
            if 'video_versions' in carousel_list[n]:
                url_video = carousel_list[n]['video_versions'][0]['url']
                
            media.append({
                'id' : values['items'][0]['carousel_media'][0]['id'],
                'type' : media_type,
                'image_url' : url_image,
                'video_url' : url_video, 
            })

    captiontext = ''
    try:
        if 'caption' in values:
            try:
                captiontext = values['items'][0]['caption']['text'].encode('ascii', 'ignore').decode('unicode-escape').replace('\n', ' ')
            except Exception as e:
                captiontext = ''
            try:
                caption = {
                    "text": captiontext,
                    "content_type": 'post' if captiontext else 'stories'
                }
            except Exception as e:
                print('error mapping')
    except Exception as e:
        print(e)
        print('caption error')

    shortcode =values['items'][0]['code']
    likecount = values['items'][0]['like_count']
    created = values['items'][0]['taken_at']
    id = values['items'][0]['pk']

    body={
        "id" : id,
        "usertags": usertags,
        "hashtags": hashtags,
        "like_count": likecount,
        "link": f"https://instagram.com/p/{shortcode}",
        "content_type": "post",
        "user": user,
        "created": convert_time(created),
        "created_at_utc": convert_time(created),
        "lastaccess": dt.now().strftime('%Y-%m-%d %H:%M:%S'),
        "media": media,
        "caption": caption,
        "location": location,
        }
    # print(body)
    filename = f"feed.json"
    with open(filename, 'w', encoding ='utf8') as json_file:
        json.dump(body, json_file, ensure_ascii = False, indent = 4)
        print('create_json feed')  
        print('')      
        
def RotateJsonFile (filename):
    if os.path.exists(filename):
        os.remove(filename)
    else:
        print("The file does not exist")

def update_profile_es():
    es = connect_elasticsearch()

    file_path = os.path.join(cwd, "profileupdate.json")
    with open(file_path, 'r') as f:
        data = json.load(f)
        user_id = data[0]["id"]
        total_follower = data[0]["total_followers"]
        total_following = data[0]["total_following"]
        print(user_id)
        print(total_follower)

    index_name = configElasticsearch.INDEX
    user_id = user_id
    search_query = {
        "query": {
            "match": {
                "user.id": user_id
            }
        }
    }

    try:
        results = es.search(index=index_name, body=search_query)
    except ConnectionError as e:
        logger.error(f"Failed to establish connection: {e}")
        raise e

    for hit in results["hits"]["hits"]:
        doc_id = hit["_id"]
        user_dict = hit["_source"]["user"]
        user_dict["total_followers"] = total_follower
        user_dict["total_following"] = total_following
        update_query = {
            "doc": {
                "user": user_dict
            }
        }
        try:
            es.update(index=index_name, id=doc_id, body=update_query, doc_type="_doc")
            print(f"Success Update Profile {doc_id}")
        except BulkIndexError as e:
            logger.error(f"Failed to update profile: {e}")
            raise e


def save_to_json(filename, result):
    with open(filename , 'w') as outfile:
        json.dump(result, outfile, indent=4)
    print(f'Data Save to Json {filename}')

def get_comments_api(shortcode, end_cursor=""):
    
    url = "https://www.instagram.com/graphql/query/?query_hash=33ba35852cb50da46f5b5e889df7d159&variables={\"shortcode\":\"" + \
        shortcode+"\",\"first\":50,\"after\":\""+end_cursor+"\"}"

    response_json = request_json(f"{url}")
    
    return response_json

def get_comments(shortcode,idpost):
    data = get_comments_api(shortcode)
    # has_next_page = data['data']['shortcode_media']['edge_media_to_comment']['page_info']['has_next_page']
    comments = []
    if 'data' in data is not None:
        has_next_page = data['data']['shortcode_media']['edge_media_to_comment']['page_info']['has_next_page']
    # if 'data' in data is not None:
        for comment in data['data']['shortcode_media']['edge_media_to_comment']['edges']:
                comment_node = comment['node']
                id = comment_node['id']
                text = comment_node['text']
                created = comment_node['created_at']
                username = comment_node['owner']['username']
                owner_id = comment_node['owner']['id']
                profile_pic_url = comment_node['owner']['profile_pic_url']
                # post_id = data['data']['shortcode_media']['id']
                comment_data = {
                    # 'in_comment_to':post_id,
                    'in_comment_to' : idpost,
                    'id': id,
                    'created': convert_time(created),
                    "caption":{
                        'text' : text,
                        'content_type':"comment"
                    },
                    'user' :{
                        'owner_id': owner_id,
                        'username': username,
                        'profile_pic_url': profile_pic_url
                    },
                    'content_type':'comment',
                    'lastaccess': dt.now().strftime('%Y-%m-%d %H:%M:%S')
                }
                comments.append(comment_data)
        while has_next_page:
            data = get_comments_api(
                shortcode, data['data']['shortcode_media']['edge_media_to_comment']['page_info']['end_cursor'])
            
            has_next_page = data['data']['shortcode_media']['edge_media_to_comment']['page_info']['has_next_page']

            for comment in data['data']['shortcode_media']['edge_media_to_comment']['edges']:
                comment_node = comment['node']
                id = comment_node['id']
                text = comment_node['text']
                created = comment_node['created_at']
                username = comment_node['owner']['username']
                owner_id = comment_node['owner']['id']
                profile_pic_url = comment_node['owner']['profile_pic_url']
                # post_id = data['data']['shortcode_media']['id']
                comment_data = {
                    # 'in_comment_to':post_id,
                    'in_comment_to' : idpost,
                    'id': id,
                    'created': convert_time(created),
                    "caption":{
                        'text' : text,
                        'content_type':"comment"
                    },
                    'user' :{
                        'owner_id': owner_id,
                        'username': username,
                        'profile_pic_url': profile_pic_url
                    },
                    'content_type':'comment',
                    'lastaccess': dt.now().strftime('%Y-%m-%d %H:%M:%S')
                }
                comments.append(comment_data)
    print("create comment")
    return comments

def extract_data():
    #read shortcode
    time_since = configTime.since
    time_until = configTime.until
    #convert to datetime
    time_since_obj = dt.strptime(time_since, '%Y-%m-%d %H:%M:%S')
    time_until_obj = dt.strptime(time_until, '%Y-%m-%d %H:%M:%S')
    file_path = os.path.join(cwd, "postfeedlink.json")
    with open(file_path, 'r') as json_file:
        values = json.load(json_file)
    for ids in range(len(values)):
        if 'link' in values[ids]:
            link = values[ids]['link']
            if 'created' in values[ids]:
                time_timestamp = values[ids]['created']
                created_dt = dt.strptime(time_timestamp, '%Y-%m-%d %H:%M:%S')
                if time_since_obj <= created_dt <= time_until_obj:
                    print("Link:", link, "Time:", created_dt)
                    url = link+'/?__a=1&__d=dis'
                    print(url)
               
                    response = request_json(f"{url}")
                    # print(response)
                    time.sleep(10)
                    # dump to json data, post feed account
                    filename = f"postfeed.json"
                    with open(filename, "w") as f:
                        json.dump(response, f, ensure_ascii=False, indent=4)
                    print("JSON data has been dumped to postfeed.json")
                    time.sleep(20)
                    parse_feed()
                    # read and send to elastic
                    UploadDatatoES(filename = 'feed.json')
                    RotateJsonFile (filename = 'feed.json')
                    RotateJsonFile (filename = 'postfeed.json')
                    time.sleep(5)
        else:
            print(f"No 'link' key found for element at index {ids}") 

def extract_data_comments(): 
    time_since = configTime.since
    time_until = configTime.until
    #convert to datetime
    time_since_obj = dt.strptime(time_since, '%Y-%m-%d %H:%M:%S')
    time_until_obj = dt.strptime(time_until, '%Y-%m-%d %H:%M:%S')
    #read shortcode
    file_path = os.path.join(cwd, "postfeedlink.json")
    with open(file_path, 'r') as json_file:
        values = json.load(json_file)
    for ids in range(len(values)):
        if 'shortcode' in values[ids]:
            shortcode = values[ids]['shortcode']
            idpost = values[ids]['id']
            if 'created' in values[ids]:
                time_timestamp = values[ids]['created']
                created_dt = dt.strptime(time_timestamp, '%Y-%m-%d %H:%M:%S')
                if time_since_obj <= created_dt <= time_until_obj:
                    print("\n shotcode:", shortcode,'with id post ',idpost, "Time:", created_dt)
                    comments = get_comments(shortcode,idpost)
                    time.sleep(5)
                    #save comments to json file
                    save_to_json(f"comments.json", comments)
                    time.sleep(5)
                    #upload data comments to elasticsearch
                    UploadDatatoESforComment(filename = 'comments.json')
                    #delete json file comments.json
                    RotateJsonFile(filename="comments.json")
            
def main():
    #login using selenium to get cookies
    login()
    driver.refresh()
    #get account target from database
    results = get_account_target()
    for row in results:
        username_target = row[0]
        print()
        print('Account :',username_target)
        time.sleep(60)

        # call function scrape_ig_post for scribe data and save data link target to postfeedlink.jason
        posts = scrape_ig_post(username = username_target)
        # df = pd.DataFrame(posts)
        # print(df)
        numberOfpost = configTime.numberOfposts
        count = 0

        # limit the number of items to the actual length of the posts list
        if numberOfpost > len(posts):
            numberOfpost = len(posts)

        with open('postfeedlink.json', 'w') as f:
            f.write('[')
            # for i, post in enumerate(posts):
            for i in range(numberOfpost):
                if i > 0:
                    f.write(',')
                f.write('\n')
                f.write(json.dumps(posts[i], indent=4))
                count += 1
            f.write('\n')
            f.write(']')      

        print(f"The amount of data that has been dumped in postfeeed.json: {count}")  
        with open('postfeedlink.json', 'r') as f:
            postsRead = json.load(f)
        df = pd.DataFrame(postsRead)
        print(df)
    
        #run fungtion extract data to extract data post
        extract_data()
        time.sleep(5)

        # run function extract data comment in every account post
        extract_data_comments()
        time.sleep(5)

        #exract profile to get follower and following
        profile = scrape_ig_profile(username= username_target)
        df = pd.DataFrame(profile)
        print(df)
        with open('profileupdate.json', 'w') as f:
            f.write(json.dumps(profile, indent=4))

        # run function to update field follower and following  
        update_profile_es()
        time.sleep(5)
        
        # update the updated field user last access
        session = SessionUser()
        user_query = session.query(Instagram).filter_by(instagram_username={username_target})
        user_query.update({'updated': datetime.utcnow()})
        session.commit()
        user = user_query.first()
        print("update user : " ,user.updated)
        session.close() 

        RotateJsonFile(filename='profileupdate.json')
        RotateJsonFile(filename='postfeedlink.json')


if __name__ == '__main__':
    main()

    
    
   

   
