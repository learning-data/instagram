from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import URL
from config import *

from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
from datetime import datetime
import json

def elasticsearch():
    es = Elasticsearch(
    [f'http://{configElasticsearch.USER}:\
        {configElasticsearch.PASSWD}@\
        {configElasticsearch.HOST}:\
        {configElasticsearch.PORT}']
    )
    return es
    # test the connection
    print(es.ping())

def connInstagramUser():
    instagramUser = URL.create(
            "mysql+pymysql",
            username= configSQL.USER,
            password= configSQL.PASSWD, 
            host= configSQL.HOST,
            database= configSQL.DATABASE,
        )
    engine = create_engine(instagramUser)

    return engine

def connInstagramAccount():
    instagramAccount = URL.create(
            "mysql+pymysql",
            username= configAccount.USER,
            password= configAccount.PASSWD, 
            host= configAccount.HOST,
            database= configAccount.DATABASE,
        )
    engine = create_engine(instagramAccount)

    return engine

# session pada connection Instagram target User Database
SessionUser = sessionmaker(bind=connInstagramUser())

# session pada connection Instagram Account Database
SessionAccount = sessionmaker(bind=connInstagramAccount())

Base = declarative_base()

