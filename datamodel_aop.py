from sqlalchemy import Column, String, Date, Integer, Numeric, TIMESTAMP
from sqlalchemy.sql import func
from base import Base

class Instagram(Base): 
    __tablename__ = 'instagram'
    id = Column(Integer, primary_key = True)
    instagram_user_id = Column(String(128))
    instagram_username = Column(String(128))
    instagram_fullname = Column(String(128))
    instagram_keyword = Column(String(128))
    type = Column(String(128))
    org_id = Column(Integer)
    status = Column(Integer)
    created = Column(TIMESTAMP)
    updated = Column(TIMESTAMP)
    is_valid = Column(Integer)

    # def __init__(self, id, instagram_user_id, instagram_username,
    #             instagram_fullname, instagram_keyword, type,
    #             org_id, status, created, updated, is_valid):
    #     self.id = id
    #     self.instagram_user_id = instagram_user_id
    #     self.instagram_username = instagram_username
    #     self.instagram_fullname = instagram_fullname
    #     self.instagram_keyword = instagram_keyword
    #     self.type = type
    #     self.org_id = org_id
    #     self.status = status
    #     self.created = created
    #     self.updated = updated
    #     self.is_valid = is_valid