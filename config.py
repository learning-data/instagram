
class configElasticsearch(object):
    USER = ''
    PASSWD = ''
    HOST = 'localhost'
    PORT = 9200
    INDEX = 'instagramtest'
    
class configAccount(object):
    PORT=3306 # default port MySQL, change if needed
    HOST="localhost"
    USER="semanticaop"
    PASSWD="Semantic123@AOP!"
    DATABASE="crawler_account"
    query = "session.query(InstagramAccount.username, InstagramAccount.password).filter_by(is_blocked=0, is_used=1).all()"

class configSQL(object):
    PORT=3306 # default port MySQL, change if needed
    HOST="localhost"
    USER="root"
    PASSWD="root"
    DATABASE="semantic_aop"

class configProxies(object):
    HTTP_PROXY = "http://43.254.124.46:3128"
    HTTPS_PROXY = "https://43.254.124.46:3128"


class configTelegramMonitor(object):
    TELEGRAM_TOKEN      = ""  #"826585762:AAGrk9yxbmxoSdZzu-QuMo5_5YAPuVk7pI0"
    TELEGRAM_CHAT_ID    ="" #"-1001398335436"
    CRAWLER_NAME        ="" #"Instagram AOP 142"

class configTime(object):
    #mulai
    since = "2023-01-01 00:00:00"
    # hingga
    until = "2023-04-11 00:00:00"
